function validateForm() {
  	var ques1= document.forms["question"]["ques1"].value;
  	var ques2= document.forms["question"]["ques2"].value;
  	var ques3= document.forms["question"]["ques3"].value;
  	var ques4= document.forms["question"]["ques4"].value;
  	var ques5= document.forms["question"]["ques5"].value;
  	var ques6= document.forms["question"]["ques6"].value;
  	var ques7= document.forms["question"]["ques7"].value;
  	var ques8= document.forms["question"]["ques8"].value;
  	var ques9= document.forms["question"]["ques9"].value;
  	var ques10= document.forms["question"]["ques10"].value;
  	var name= document.forms["question"]["name"].value;

  	//to check if the question have been answered or not
  	if(ques1==""){
  		alert("Oops!! Question 1 is required");
  		return false;
  	}
  	else if(ques2==""){
  		alert("Oops!! Question 2 is required");
  		return false;
  	}
  	else if(ques3==""){
  		alert("Oops!! Question 3 is required");
  		return false;
  	}
  	else if(ques4==""){
  		alert("Oops!! Question 4 is required");
  		return false;
  	}
  	else if(ques5==""){
  		alert("Oops!! Question 5 is required");
  		return false;
  	}
  	else if(ques6==""){
  		alert("Oops!! Question 6 is required");
  		return false;
  	}
  	else if(ques7==""){
  		alert("Oops!! Question 7 is required");
  		return false;
  	}
  	else if(ques8==""){
  		alert("Oops!! Question 8 is required");
  		return false;
  	}
  	else if(ques9==""){
  		alert("Oops!! Question 9 is required");
  		return false;
  	}
  	else if(ques10==""){
  		alert("Oops!! Question 10 is required");
  		return false;
  	}

  	//to check correct answer and count score
  	var score=0;

  	if(ques1=="Cascading Style Sheets"){
  		score++;
  	}
  	if(ques2=="style"){
  		score++;
  	}
  	if(ques3=="background-color"){
  		score++;
  	}
  	if(ques4=="color"){
  		score++;
  	}
  	if(ques5=="font-size"){
  		score++;
  	}
  	if(ques6=="font-weight:bold;"){
  		score++;
  	}
  	if(ques7=="Separate each selector with a comma"){
  		score++;
  	}
  	if(ques8=="static"){
  		score++;
  	}
  	if(ques9=="#demo"){
  		score++;
  	}
  	if(ques10=="No"){
  		score++;
  	}
  	//to display score
  	if(score<=4 && score>=0){
  		alert("Keep trying, "+name+"! You answered "+ score+ " out of 10 correctly");
  	}
  	else if( score<=9 && score>=5){
  		alert("Way to go, "+name+"! You got "+ score+ " out of 10 correct");
  	}
  	else if(score==10){
  		alert("Congratulation "+name+"! You got "+ score+ " out of 10");
  	}
  }